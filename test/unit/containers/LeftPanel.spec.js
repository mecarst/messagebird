import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { assert, expect } from 'chai';
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import LeftPanel from '../../../app/js/containers/LeftPanel';

describe('LeftPanel Container', () => {
	let wrapper, mockStore;

	beforeEach(() => {
		mockStore = createStore(() => {});
		wrapper = shallow(<Provider store={mockStore} ><LeftPanel /></Provider>);
	});

	it('should be rendered', () => {
		expect(wrapper).to.have.length(1);
		expect(wrapper.find(LeftPanel).length).to.equal(1);
  	});
});