const router = require('express').Router();
const messagebird = require('./messagebird');

router.use(messagebird);

module.exports = router;
