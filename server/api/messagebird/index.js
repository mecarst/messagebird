const router = require('express').Router();
const smsService = require('./sms.services');

router
	.get('/', (req, res) => {
		res.render('index', { 
			title: 'Messagebird-API', 
			APIuri: `api`
		});
	})
    .get('/api/messages', smsService.getSMS)
    .post('/api/messages', smsService.sendSMS)

module.exports = router;
