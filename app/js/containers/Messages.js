import React, { Component }   from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import MessagesContent from '../components/MessagesContent';

class Messages extends Component {

    render() {
        return (
            <div className="messages">
           		<MessagesContent />
            </div>
        );
    }
}

export default connect(state => ({ state}),
    dispatch => bindActionCreators({}, dispatch))
(Messages);