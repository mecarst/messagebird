import config from '../../../config/config'

function get(url, json = true) {
    return fetch(url)
        .then(res => !res.ok ? new Error(res.statusText) : res)
        .then(res => (json && !(res instanceof Error)) ? res.json() : res)
}

function post(url, body, json) {
    return fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Authorization': `AccessKey ${config.APIKey}`,
                'Content-Type': 'application/json'
            },
        })
        .then(res => !res.ok ? new Error(res.statusText) : res)
        .then(res => (json && !(res instanceof Error)) ? res.json() : res);
}

export default {
    getSms() {
        return get(`${config.APIuri}/messages?access_key=${config.APIKey}`);
    },
    sendSms(sms) {
        return post(`${config.APIuri}/messages`, sms, true);
    }
};
