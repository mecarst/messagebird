import React, { Component }   from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import DashboardContent from '../components/DashboardContent';

class Dashboard extends Component {

    render() {
        return (
            <div className="dashboard">
			    <DashboardContent smsItems={this.props.state.sms.items} />
            </div>
        );
    }
}

export default connect(state => ({ state}),
    dispatch => bindActionCreators({}, dispatch))
(Dashboard);
