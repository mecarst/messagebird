import * as ActionConstants from './sms.actions.constants';

//load sms's
export function loadSMS() {
    return { type: ActionConstants.LOAD_SMS };
}

export function smsLoaded(sms) {
    return { type: ActionConstants.SMS_LOADED, sms };
}

export function smsLoadedFailed(err) {
    return { type: ActionConstants.SMS_LOAD_FAILED, err };
}

//send
export function sendMS(sms) {
    return { type: ActionConstants.SEND_SMS, sms };
}

export function smsSent(res) {
    return { type: ActionConstants.SEND_SMS_SENT, res };
}

export function smsSentFailed(err) {
    return { type: ActionConstants.SEND_SMS_FAILED, err };
}


