const http = require('http');
const url = require('url');
const config = require('../../config/config');

exports.copyHeaders = (source, target) => {
    for (let header in source.headers) {
        target.setHeader(header, source.headers[header]);
    }
    target.setHeader('access-control-allow-origin', '*');
};

exports.proxyRequests = (resourceUrl, req, res, next) => {
    const { hostname } = url.parse(`${resourceUrl}`);
    const { method, originalUrl } = req;
	const uri = originalUrl.replace('/api', '');
    const serverRequest = http.request({
        hostname, method,
        path: `${uri}?access_key=${config.APIKey}`
    });

    return new Promise((resolve, reject) => {
    	exports.copyHeaders(req, serverRequest);

    	serverRequest.on('response', serverResponse => {
	        res.statusCode = serverResponse.statusCode;
	        exports.copyHeaders(serverResponse, res);
	        serverResponse.pipe(res);
	    });

	    serverRequest.on('error', e => {
	        var error = new Error('request failed: ' + e.message);
      		if (error.message === 'ECONNRESET') error = new Error('request timeout');
	        
	        reject(res.end());
	    });

	    resolve(req.pipe(serverRequest));
    });
}
