import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export default class Footer extends Component {
    render() {
        return (
            <footer>
            	Contact | About Us | Privacy
            </footer>
        );
    }
}

