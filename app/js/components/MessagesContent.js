import React, { Component }   from 'react';
import { bindActionCreators, store } from 'redux';
import { connect }            from 'react-redux';

import * as smsActions from '../modules/sms/sms.actions.creators';

const errorStyle = {
  color: 'red',
  marginTop: "20px"
};

const sentStyle = {
  color: 'green',
  marginTop: "20px"
}

class MessagesContent extends Component {
	constructor(props) {
	    super(props);

	    this.state = {
	    	recipients: '',
	    	originator: '',
	    	body: ''
	    };

	    this.handleInputChange = this.handleInputChange.bind(this);
	    this.sendMessage = this.sendMessage.bind(this);
	}

	handleInputChange(e) {
		const {target: {value, name}} = e;

	    this.setState({
	      	[name]: value
	    });
	}	
	
	sendMessage(e) {
	    e.preventDefault();

	    const {recipients, originator, body } = this.state;
	    if (!recipients || !originator || !body) return false;

	    this.props.sendMS({recipients, originator, body});

	    this.setState({
	    	recipients: '',
	    	originator: '',
	    	body: ''
	    })
	}

    render() {
		const {smsSendError, sentSms} = this.props.state.sms;

        return (
            <div className="content">
	            <div className="sms-send">
	              	<input
		            	type="text"
		            	name="recipients"
                       	className="sms_recipient"
		            	placeholder="Recipient:"
		            	value={this.state.recipients}
		            	onChange={this.handleInputChange} 
	            	/>
		          	<input
		            	type="text"
		            	name="originator"
	                    className="sms_originator"   
                       	placeholder="Originator:"
		            	value={this.state.originator}
		            	onChange={this.handleInputChange} 
	            	/>
	                <textarea 
                		name="body"
                		placeholder="Message" 
                       	value={this.state.body}
                		onChange={this.handleInputChange}  
                    />
	                <button onClick={this.sendMessage}>Send SMS</button>

	            	{smsSendError && 
		        		<div style={errorStyle}> There was an error sending your SMS!</div>
	    	    	}

	        	    {sentSms &&
	            		<div style={sentStyle}>SMS succefully sent!</div>
	            	}	
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({ state }),
    dispatch => bindActionCreators(smsActions, dispatch)
)(MessagesContent);

export { MessagesContent }


// export default connect(state => ({ state}),
//     dispatch => bindActionCreators(smsActions, dispatch))
// (MessagesContent);

// export { MessagesContent }