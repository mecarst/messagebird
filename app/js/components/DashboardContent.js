import React, { Component }   from 'react';

export default class DashboardContent extends Component {

    render() {
    	const { smsItems } = this.props;

        return (
            <div className="content">
            	<div className="sms-list-holder">
	        		<div className="sms-list header">
						<div>Type</div>
						<div>Originator</div>
						<div>Body</div>
						<div>Datetime</div>
					</div>
	            	{smsItems.map((sms, i) =>
	            		<div className="sms-list" key={i}>
	    					<div>{sms.type}</div>
	    					<div>{sms.originator}</div>
	    					<div>{sms.body}</div>
	    					<div>{(new Date(sms.createdDatetime)).toString()}</div>
	    				</div>
	                )}
                </div>
            </div>
        );
    }
}
