import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LeftPanel extends Component {
    render() {
        return (
            <div className="left-panel">
                <Link to="/">Dashboard</Link>
                <Link to="/sms">SMS</Link>
        	</div>
        );
    }
}

export default LeftPanel;