## How to:
- `npm install yarn -g` in preferred command line
- `yarn install`, install packages
- `yarn start`, to start new web server
- Go to [localhost:3004](http://localhost:3004)


## Documentation

### Local Development
- `yarn serve`, to start a local web-dev-server
- Go to [localhost:8080](http://localhost:8080)

### Build bundle
- `yarn build`

### Runing tests
- `yarn test`

### Start prod server
- `yarn start`

### Start dev server
- `yarn start:dev`


### Webpack

The application uses webpack for createing the bundle js and css. Also runnig the dev-server for development mode.

### ExpressJS

It is used for starting the application on localhost:3004 and running the api calls to get data from the API.

### App

The application folder consists of javascript and styles.
Main entry point of the application is app/js/index.js.

### Api

The API folder as described above, serves the purpose of creating API calls for different routes.
