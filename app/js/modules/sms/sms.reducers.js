import * as ActionConstants from './sms.actions.constants';

const initialState = {
    items: [],
    smsSendError: null,
    sentSms: null
};

function setSmsFailed(state, action){
    const newState = {...state};

    newState.sentSms = null;
    newState.smsSendError = action.err.message;

    return newState;
}

function setSms(state, action){
    const newState = {...state};

    newState.sentSms = action.res;
    return newState;
}

export function sms(state = initialState, action) {

    switch(action.type) {
        case ActionConstants.LOAD_SMS:
            return {
                ...state
            };
        case ActionConstants.SMS_LOADED:
            return {
                ...state,
                items: state.items.concat(action.sms.items)
            };
        case ActionConstants.SMS_LOAD_FAILED:
            return {
                items: [],
                smsLoadError: action.err
            };
        case ActionConstants.SEND_SMS:
            return {
                ...state
            };
        case ActionConstants.SEND_SMS_SENT:
            return setSms(state, action);
        case ActionConstants.SEND_SMS_FAILED:
            return setSmsFailed(state, action);
        default:
            return state;
    }

}