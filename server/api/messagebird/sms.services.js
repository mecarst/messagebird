const config = require('../../../config/config');
const utils = require('../../utils/index.js');

exports.getSMS = (req, res, next) => {
	return utils.proxyRequests(`${config.APIuri}/messages`, req, res, next)
		.then(items => res.status(200).json(items))
		.catch(err => {
            console.log("ERROR ", err);
            return err;
        });
};

exports.sendSMS = (req, res, next) => {
	const sms = req.body;
  	return utils.proxyRequests()(`${config.APIuri}/messages`, sms, true, req, res, next)
    	.then(sms => res.status(200).json(sms))
        .catch(err => {
            console.log("ERROR ", err);
            return err;
        });
}


