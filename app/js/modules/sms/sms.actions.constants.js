export const LOAD_SMS = 'sms/LOAD_SMS';
export const SMS_LOADED = 'sms/SMS_LOADED';
export const SMS_LOAD_FAILED = 'sms/SMS_LOAD_FAILED';


export const SEND_SMS = 'sms/SEND_SMS';
export const SEND_SMS_SENT = 'sms/SEND_SMS_SENT';
export const SEND_SMS_FAILED = 'sms/SEND_SMS_FAILED ';
