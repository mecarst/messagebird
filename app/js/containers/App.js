import React, { Component }   	from 'react';
import { bindActionCreators } 	from 'redux';
import { connect }            	from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import * as smsActions from '../modules/sms/sms.actions.creators';

import Header 		from './Header';
import Dashboard 	from './Dashboard';
import Messages 	from './Messages';
import LeftPanel 	from './LeftPanel';
import Footer 		from './Footer';

import HeaderLayout from '../components/layout/HeaderLayout';
import LeftLayout from '../components/layout/LeftLayout';
import FooterLayout from '../components/layout/FooterLayout';

class App extends Component {
    
    componentDidMount() {
        const { state: { sms: {smsItems} }} = this.props;
        this.props.loadSMS();
    }

    render() {
        return (
        	<Router>
                <div className="app-root">
                	<HeaderLayout>
                		<Header />
                	</HeaderLayout>
                    
                    <div className="content-wrapper">
                        <LeftLayout>
                            <LeftPanel />
                        </LeftLayout>
                        
                        <Route exact path="/" component={Dashboard} />
                        <Route path="/sms" component={Messages}/>
                    </div>

					<FooterLayout>
                		<Footer />
                	</FooterLayout>
                </div>
            </Router>
        );
    }
}

export default connect(state => ({ state}),
    dispatch => bindActionCreators({...smsActions }, dispatch))
(App);
