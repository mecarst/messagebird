import { takeLatest, call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import api from '../../services';

import * as SmsActions from './sms.actions.creators';
import * as ActionConstants from './sms.actions.constants';

export function* watchSmsLoad() {
    yield takeLatest(ActionConstants.LOAD_SMS, handleSmsLoad);
}

function* handleSmsLoad(action) {
    try {
        let sms = yield call(api.getSms);
        if (sms && (sms instanceof Error)) yield put(SmsActions.smsLoadedFailed(e.message));

        yield put(SmsActions.smsLoaded(sms));
    } catch (e) {
        yield put(SmsActions.smsLoadedFailed(e.message));
    }
}

export function* watchSmsSend() {
    yield takeLatest(ActionConstants.SEND_SMS, handleSmsSend);
}

function* handleSmsSend(action) {
    try {
        let sms = yield call(api.sendSms, action.sms);
        if (sms && (sms instanceof Error)) yield put(SmsActions.smsSentFailed(sms));
        
        yield put(SmsActions.smsSent(sms));
    } catch (e) {
        yield put(SmsActions.smsSentFailed(e));
    }
}
